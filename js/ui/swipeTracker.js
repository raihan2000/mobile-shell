// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-

import Clutter from 'gi://Clutter';
import Gio from 'gi://Gio';
import GObject from 'gi://GObject';
import Mtk from 'gi://Mtk';

import * as Main from './main.js';
import * as Params from '../misc/params.js';

// FIXME: ideally these values matches physical touchpad size. We can get the
// correct values for gnome-shell specifically, since mutter uses libinput
// directly, but GTK apps cannot get it, so use an arbitrary value so that
// it's consistent with apps.
const TOUCHPAD_BASE_HEIGHT = 300;
const TOUCHPAD_BASE_WIDTH = 400;

const MIN_ANIMATION_DURATION = 100;
const MAX_ANIMATION_DURATION = 400;
const VELOCITY_THRESHOLD_TOUCH = 0.3;
const VELOCITY_THRESHOLD_TOUCHPAD = 0.6;
const DECELERATION_TOUCH = 0.998;
const DECELERATION_TOUCHPAD = 0.997;
const VELOCITY_CURVE_THRESHOLD = 2;
const DECELERATION_PARABOLA_MULTIPLIER = 0.35;

// Derivative of easeOutCubic at t=0
const DURATION_MULTIPLIER = 3;
const ANIMATION_BASE_VELOCITY = 0.002;
const EPSILON = 0.005;

const GESTURE_FINGER_COUNT = 3;

// USAGE:
//
// To correctly implement the gesture, there must be handlers for the following
// signals:
//
// begin(tracker, monitor)
//   The handler should check whether a deceleration animation is currently
//   running. If it is, it should stop the animation (without resetting
//   progress). Then it should call:
//   tracker.confirmSwipe(distance, snapPoints, currentProgress, cancelProgress)
//   If it's not called, the swipe would be ignored.
//   The parameters are:
//    * distance: the page size;
//    * snapPoints: an (sorted with ascending order) array of snap points;
//    * currentProgress: the current progress;
//    * cancelprogress: a non-transient value that would be used if the gesture
//      is cancelled.
//   If no animation was running, currentProgress and cancelProgress should be
//   same. The handler may set 'orientation' property here.
//
// update(tracker, progress)
//   The handler should set the progress to the given value.
//
// end(tracker, duration, endProgress)
//   The handler should animate the progress to endProgress. If endProgress is
//   0, it should do nothing after the animation, otherwise it should change the
//   state, e.g. change the current page or switch workspace.
//   NOTE: duration can be 0 in some cases, in this case it should finish
//   instantly.

/** A class for handling swipe gestures */
export const SwipeTracker = GObject.registerClass({
    Properties: {
        'orientation': GObject.ParamSpec.enum(
            'orientation', 'orientation', 'orientation',
            GObject.ParamFlags.READWRITE,
            Clutter.Orientation, Clutter.Orientation.HORIZONTAL),
        'distance': GObject.ParamSpec.double(
            'distance', 'distance', 'distance',
            GObject.ParamFlags.READWRITE,
            0, Infinity, 0),
        'allow-long-swipes': GObject.ParamSpec.boolean(
            'allow-long-swipes', 'allow-long-swipes', 'allow-long-swipes',
            GObject.ParamFlags.READWRITE,
            false),
        'scroll-modifiers': GObject.ParamSpec.flags(
            'scroll-modifiers', 'scroll-modifiers', 'scroll-modifiers',
            GObject.ParamFlags.READWRITE,
            Clutter.ModifierType, 0),
    },
    Signals: {
        'swipe-begin':  {param_types: [GObject.TYPE_UINT]},
        'swipe-update': {param_types: [GObject.TYPE_DOUBLE]},
        'swipe-end':    {param_types: [GObject.TYPE_UINT64, GObject.TYPE_DOUBLE, GObject.TYPE_JSOBJECT]},
    },
}, class SwipeTracker extends Clutter.PanGesture {
    _init(orientation, allowedModes, params) {
        params = Params.parse(params, {
            allowDrag: true,
            allowScroll: true,
            pickupOnPress: true,
        });

        super._init({
            pan_axis: orientation === Clutter.Orientation.HORIZONTAL
                ? Clutter.PanAxis.X : Clutter.PanAxis.Y,
            min_n_points: params.allowDrag ? 1 : GESTURE_FINGER_COUNT,
            max_n_points: params.allowDrag ? 0 : GESTURE_FINGER_COUNT,
        });

        this.orientation = orientation;
        this._allowedModes = allowedModes;
        this._enabled = true;
        this._distance = global.screen_height;

        this._snapPoints = [];
        this._initialProgress = 0;
        this._cancelProgress = 0;
        this._progress = 0;

        this.connect('recognize', this._beginPanGesture.bind(this));
        this.connect('pan-update', this._updatePanGesture.bind(this));
        this.connect('end', this._endPanGesture.bind(this));
        this.connect('cancel', this._cancelPanGesture.bind(this));

        this._scrollEnabled = params.allowScroll;
        this._pickupOnPress = params.pickupOnPress;

        this.connect('notify::enabled', () => {
            if (!this.enabled && this.state === Clutter.GestureState.RECOGNIZING)
                this.set_state(Clutter.GestureState.CANCELLED);
        });
    }

    vfunc_may_recognize() {
        return (this._allowedModes & Main.actionMode) !== 0;
    }

    /**
     * canHandleScrollEvent:
     * This function can be used to combine swipe gesture and mouse
     * scrolling.
     *
     * @param {Clutter.Event} scrollEvent an event to check
     * @returns {boolean} whether the event can be handled by the tracker
     */
    canHandleScrollEvent(event) {
        if (!this.enabled || !this._scrollEnabled)
            return false;

        if (event.type() !== Clutter.EventType.SCROLL)
            return false;

        if (event.get_scroll_source() !== Clutter.ScrollSource.FINGER &&
            event.get_source_device().get_device_type() !== Clutter.InputDeviceType.TOUCHPAD_DEVICE)
            return false;

        if ((this._allowedModes & Main.actionMode) === 0)
            return false;

        if (this.state !== Clutter.GestureState.RECOGNIZING && this.scrollModifiers !== 0 &&
            (event.get_state() & this.scrollModifiers) === 0)
            return false;

        return true;
    }

    get distance() {
        return this._distance;
    }

    set distance(distance) {
        if (this._distance === distance)
            return;

        this._distance = distance;
        this.notify('distance');
    }

    _beginGesture(x, y) {
        this.set_pickup_on_press(false);

        const rect = new Mtk.Rectangle({x, y, width: 1, height: 1});
        let monitor = global.display.get_monitor_index_for_rect(rect);

        this.emit('swipe-begin', monitor);
    }

    _beginPanGesture(panGesture) {
        const coords = panGesture.get_begin_centroid(null);

        this._beginGesture(coords.x, coords.y);
    }

    _findClosestPoint(pos) {
        const distances = this._snapPoints.map(x => Math.abs(x - pos));
        const min = Math.min(...distances);
        return distances.indexOf(min);
    }

    _findNextPoint(pos) {
        return this._snapPoints.findIndex(p => p >= pos);
    }

    _findPreviousPoint(pos) {
        const reversedIndex = this._snapPoints.slice().reverse().findIndex(p => p <= pos);
        return this._snapPoints.length - 1 - reversedIndex;
    }

    _findPointForProjection(pos, velocity) {
        const initial = this._findClosestPoint(this._initialProgress);
        const prev = this._findPreviousPoint(pos);
        const next = this._findNextPoint(pos);

        // If the swipe was so small that the projection landed on the current
        // page, we still choose the next page (feels better in real use).
        if ((velocity > 0 ? prev : next) === initial)
            return velocity > 0 ? next : prev;

        return this._findClosestPoint(pos);
    }

    _updatePanGesture(panGesture, deltaX, deltaY, pannedDistance) {
        let delta = this.orientation === Clutter.Orientation.HORIZONTAL
            ? -deltaX
            : -deltaY;

        if (this.orientation === Clutter.Orientation.HORIZONTAL &&
            Clutter.get_default_text_direction() === Clutter.TextDirection.RTL)
            delta = -delta;

        const lastEventType = this.get_point_event(-1).type();
        const isTouchpad = lastEventType === Clutter.EventType.TOUCHPAD_SWIPE ||
            lastEventType === Clutter.EventType.SCROLL;

        const vertical = this.orientation === Clutter.Orientation.VERTICAL;
        const distance = isTouchpad
            ? vertical ? TOUCHPAD_BASE_HEIGHT : TOUCHPAD_BASE_WIDTH
            : this._distance;

        if (distance === 0)
            throw new Error();

        this._progress += delta / distance;

        const prevPoint = this._findPreviousPoint(this._progress);
        const nextPoint = this._findNextPoint(this._progress);
        if (prevPoint < this._peekedMinSnapPoint)
            this._peekedMinSnapPoint = prevPoint;
        if (nextPoint > this._peekedMaxSnapPoint)
            this._peekedMaxSnapPoint = nextPoint;

        this._progress = Math.clamp(this._progress, this._snapPoints[0], this._snapPoints[this._snapPoints.length - 1]);

        this.emit('swipe-update', this._progress);
    }

    _getEndProgress(velocity, distance, isTouchpad) {
        const threshold = isTouchpad ? VELOCITY_THRESHOLD_TOUCHPAD : VELOCITY_THRESHOLD_TOUCH;

        if (Math.abs(velocity) < threshold)
            return this._snapPoints[this._findClosestPoint(this._progress)];

        const decel = isTouchpad ? DECELERATION_TOUCHPAD : DECELERATION_TOUCH;
        const slope = decel / (1.0 - decel) / 1000.0;

        let pos;
        if (Math.abs(velocity) > VELOCITY_CURVE_THRESHOLD) {
            const c = slope / 2 / DECELERATION_PARABOLA_MULTIPLIER;
            const x = Math.abs(velocity) - VELOCITY_CURVE_THRESHOLD + c;

            pos = slope * VELOCITY_CURVE_THRESHOLD +
                DECELERATION_PARABOLA_MULTIPLIER * x * x -
                DECELERATION_PARABOLA_MULTIPLIER * c * c;
        } else {
            pos = Math.abs(velocity) * slope;
        }

        pos = pos * Math.sign(velocity) + this._progress;
        const boundsMin = this.allowLongSwipes
            ? this._snapPoints[0] : this._snapPoints[this._peekedMinSnapPoint];
        const boundsMax = this.allowLongSwipes
            ? this._snapPoints[this._snapPoints.length - 1]
            : this._snapPoints[this._peekedMaxSnapPoint];
        pos = Math.clamp(pos, boundsMin, boundsMax);

        const index = this._findPointForProjection(pos, velocity);

        return this._snapPoints[index];
    }

    _getAnimateOutParams(velocity, isTouchpad) {
        const vertical = this.orientation === Clutter.Orientation.VERTICAL;
        const distance = isTouchpad
            ? vertical ? TOUCHPAD_BASE_HEIGHT : TOUCHPAD_BASE_WIDTH
            : this._distance;

        const endProgress = this._getEndProgress(velocity, distance, isTouchpad);

        velocity /= distance;

        if ((endProgress - this._progress) * velocity <= 0)
            velocity = ANIMATION_BASE_VELOCITY;

        const nPoints = Math.max(1, Math.ceil(Math.abs(this._progress - endProgress)));
        const maxDuration = MAX_ANIMATION_DURATION;

        let duration = Math.abs((this._progress - endProgress) / velocity * DURATION_MULTIPLIER);
        if (duration > 0)
            duration = Math.clamp(duration, MIN_ANIMATION_DURATION, maxDuration);

        return [duration, endProgress];
    }

    _endAnimationDoneCb() {
        this.set_pickup_on_press(false);
    }

    _endGesture(velocity) {
        const lastEventType = this.get_point_event(-1).type();
        const isTouchpad = lastEventType === Clutter.EventType.TOUCHPAD_SWIPE ||
            lastEventType === Clutter.EventType.SCROLL;

        if (this._otherDimensionTracker) {
            const otherTracker = this._otherDimensionTracker;

            if (otherTracker.state === Clutter.GestureState.RECOGNIZING) {
                this._endVelocity = velocity;
                this._endIsTouchpad = isTouchpad;
                return;
            }

            if (otherTracker._endVelocity !== undefined) {
                if (Math.abs(velocity) < Math.abs(otherTracker._endVelocity))
                    velocity = 0;
                else
                    otherTracker._endVelocity = 0;

                const [ourDuration, ourEndProgress] = this._getAnimateOutParams(velocity, isTouchpad);
                const [otherDuration, otherEndProgress] =
                    otherTracker._getAnimateOutParams(otherTracker._endVelocity, otherTracker._endIsTouchpad);

                const finalDuration = ourDuration > otherDuration ? ourDuration : otherDuration;

                if (this.get_pickup_on_press())
                    throw new Error("tracker already has pickup on press");

                if (otherTracker.get_pickup_on_press())
                    throw new Error("otherTracker already has pickup on press");

                if (this._pickupOnPress) {
                    this.set_pickup_on_press(true);
                    otherTracker.set_pickup_on_press(true);
                }

                otherTracker.emit('swipe-end', finalDuration, otherEndProgress, this._endAnimationDoneCb.bind(otherTracker));
                this.emit('swipe-end', finalDuration, ourEndProgress, this._endAnimationDoneCb.bind(this));

                delete otherTracker._endVelocity;
                delete otherTracker._endIsTouchpad;

                return;
            }
        }

        const [duration, endProgress] = this._getAnimateOutParams(velocity, isTouchpad);

        if (this.get_pickup_on_press())
            throw new Error("sw tracker already has pickup on press");

        if (this._pickupOnPress)
            this.set_pickup_on_press(true);

        this.emit('swipe-end', duration, endProgress, this._endAnimationDoneCb.bind(this));
    }

    _endPanGesture(panGesture) {
        const velocity = panGesture.get_velocity();
        const v = this.orientation === Clutter.Orientation.HORIZONTAL
            ? -velocity.get_x()
            : -velocity.get_y();

        this._endGesture(v);
    }

    _cancelPanGesture() {
        this._endGesture(0);
    }

    /**
     * Confirms a swipe. User has to call this in 'begin' signal handler,
     * otherwise the swipe wouldn't start. If there's an animation running,
     * it should be stopped first.
     *
     * `cancelProgress` must always be a snap point, or a value matching
     * some other non-transient state.
     *
     * @param {number} distance - swipe distance in pixels
     * @param {number[]} snapPoints - An array of snap points, sorted in ascending order
     * @param {number} currentProgress - initial progress value
     * @param {number} cancelProgress - the value to be used on cancelling
     * @param {number} animatingTowardsProgress - the "target progress" of an ongoing animation
     */
    confirmSwipe(distance, snapPoints, currentProgress, cancelProgress, animatingTowardsProgress = null) {
        this.distance = distance;
        this._snapPoints = snapPoints;
        this._initialProgress = currentProgress;
        this._progress = currentProgress;
        this._cancelProgress = cancelProgress;

        this._peekedMinSnapPoint = this._findPreviousPoint(this._progress);
        this._peekedMaxSnapPoint = this._findNextPoint(this._progress);

        if (animatingTowardsProgress &&
            this._peekedMinSnapPoint > 0 &&
            animatingTowardsProgress === this._snapPoints[this._peekedMinSnapPoint])
            this._peekedMinSnapPoint--;

        if (animatingTowardsProgress &&
            this._peekedMaxSnapPoint < this._snapPoints.length - 1 &&
            animatingTowardsProgress === this._snapPoints[this._peekedMaxSnapPoint])
            this._peekedMaxSnapPoint++;
    }

    make2d(otherSwipeTracker) {
        if (!(otherSwipeTracker instanceof SwipeTracker))
            throw new Error('Must pass a SwipeTracker to make2d');

        if (this.orientation === otherSwipeTracker.orientation)
            throw new Error('Other SwipeTracker must have different orientation');

        if (this._otherDimensionTracker || otherSwipeTracker._otherDimensionTracker)
            throw new Error('Is already 2d');

        this.can_not_cancel(otherSwipeTracker);
        otherSwipeTracker.can_not_cancel(this);

        this._otherDimensionTracker = otherSwipeTracker;
        otherSwipeTracker._otherDimensionTracker = this;
    }

    vfunc_should_influence(otherGesture, cancel, inhibit) {
        if (this.get_points().length === 3 && (this.actor !== otherGesture.actor && this.actor.contains(otherGesture.actor))) {
log(this + " length 3, inhibit true");
            return [cancel, true];
}

        return [cancel, inhibit]
    }
});
