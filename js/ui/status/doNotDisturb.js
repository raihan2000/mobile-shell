import Gio from 'gi://Gio';
import GObject from 'gi://GObject';

import {QuickToggle, SystemIndicator} from '../quickSettings.js';

const DoNotDisturbToggle = GObject.registerClass(
class DoNotDisturbToggle extends QuickToggle {
    _init() {
        super._init({
            title: _('Do Not Disturb'),
        });

        this._settings = new Gio.Settings({
            schema_id: 'org.gnome.desktop.notifications',
        });

        this._settings.connect('changed::show-banners', () => this._syncIcon());
        this._settings.bind('show-banners',
            this, 'checked',
            Gio.SettingsBindFlags.INVERT_BOOLEAN);

        this.connectObject(
            'destroy', () => this._settings.run_dispose(),
            'clicked', () => this._settings.set_boolean('show-banners', this.checked),
            this);

        this._syncIcon();
    }

    _syncIcon() {
        this.icon_name = this._settings.get_boolean('show-banners')
            ? 'no-notifications-symbolic' : 'notifications-disabled-symbolic';
    }
});

export const Indicator = GObject.registerClass(
class Indicator extends SystemIndicator {
    _init() {
        super._init();

        this.quickSettingsItems.push(new DoNotDisturbToggle());
    }
});
