// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-

import Clutter from 'gi://Clutter';
import GLib from 'gi://GLib';
import GObject from 'gi://GObject';
import Meta from 'gi://Meta';
import Mtk from 'gi://Mtk';
import St from 'gi://St';

import * as Main from './main.js';
import * as SwipeTracker from './swipeTracker.js';

const EDGE_THRESHOLD = 35;
const DRAG_DISTANCE = 80;
const CANCEL_THRESHOLD = 100;
const CANCEL_TIMEOUT_MS = 300;

export const EdgeDragAction = GObject.registerClass({
    Signals: {
        'progress': {param_types: [GObject.TYPE_DOUBLE]},
    },
}, class EdgeDragAction extends Clutter.Gesture {
    _init(side, allowedModes) {
        super._init();
        this._side = side;
        this._allowedModes = allowedModes;
    }

    _getMonitorForCoords(coords) {
        const rect = new Mtk.Rectangle({x: coords.x - 1, y: coords.y - 1, width: 1, height: 1});
        const monitorIndex = global.display.get_monitor_index_for_rect(rect);

        return Main.layoutManager.monitors[monitorIndex];
    }

    _isNearMonitorEdge(point) {
        const { scaleFactor } = St.ThemeContext.get_for_stage(global.stage);
        const latestCoords = this.get_point_coords(point, null);
        const monitor = this._getMonitorForCoords(latestCoords);

        switch (this._side) {
        case St.Side.LEFT:
            return latestCoords.x < monitor.x + (EDGE_THRESHOLD * scaleFactor);
        case St.Side.RIGHT:
            return latestCoords.x > monitor.x + monitor.width - (EDGE_THRESHOLD * scaleFactor);
        case St.Side.TOP:
            return latestCoords.y < monitor.y + (EDGE_THRESHOLD * scaleFactor);
        case St.Side.BOTTOM:
            return latestCoords.y > monitor.y + monitor.height - (EDGE_THRESHOLD * scaleFactor);
        }
    }

    _exceedsCancelThreshold(point) {
        const { scaleFactor } = St.ThemeContext.get_for_stage(global.stage);
        const beginCoords = this.get_point_begin_coords(point, null);
        const [_distance, offsetX, offsetY] =
            beginCoords.distance(this.get_point_coords(point, null));

        switch (this._side) {
        case St.Side.LEFT:
        case St.Side.RIGHT:
            return offsetY > CANCEL_THRESHOLD * scaleFactor;
        case St.Side.TOP:
        case St.Side.BOTTOM:
            return offsetX > CANCEL_THRESHOLD * scaleFactor;
        }
    }

    _passesDistanceNeeded(point) {
        const { scaleFactor } = St.ThemeContext.get_for_stage(global.stage);
        const monitor = this._getMonitorForCoords(this.get_point_begin_coords(point, null));
        const latestCoords = this.get_point_coords(point, null);

        switch (this._side) {
        case St.Side.LEFT:
            return latestCoords.x > monitor.x + (DRAG_DISTANCE * scaleFactor);
        case St.Side.RIGHT:
            return latestCoords.x < monitor.x + monitor.width - (DRAG_DISTANCE * scaleFactor);
        case St.Side.TOP:
            return latestCoords.y > monitor.y + (DRAG_DISTANCE * scaleFactor);
        case St.Side.BOTTOM:
            return latestCoords.y < monitor.y + monitor.height - (DRAG_DISTANCE * scaleFactor);
        }
    }

    vfunc_should_handle_sequence(sequenceBeginEvent) {
        const eventType = sequenceBeginEvent.type();

        return eventType === Clutter.EventType.TOUCH_BEGIN;
    }

    vfunc_point_began(point) {
        const nPoints = this.get_n_points();

        if (nPoints > 1 ||
            !(this._allowedModes & Main.actionMode) ||
            !this._isNearMonitorEdge(point)) {
            this.set_state(Clutter.GestureState.CANCELLED);
            return;
        }

        this._cancelTimeoutId =
            GLib.timeout_add(GLib.PRIORITY_DEFAULT, CANCEL_TIMEOUT_MS, () => {
                if (this._isNearMonitorEdge(point))
                    this.set_state(Clutter.GestureState.CANCELLED);

                delete this._cancelTimeoutId;
                return GLib.SOURCE_REMOVE;
            });
    }

    vfunc_point_moved(point) {
        if (this._exceedsCancelThreshold(point)) {
            this.set_state(Clutter.GestureState.CANCELLED);
            return;
        }

        if (this.state === Clutter.GestureState.POSSIBLE &&
            !this._isNearMonitorEdge(point))
            this.set_state(Clutter.GestureState.RECOGNIZING);
    
        if (this.state === Clutter.GestureState.RECOGNIZING) {
            const beginCoords = this.get_point_begin_coords(point, null);
            const [_distance, offsetX, offsetY] =
                beginCoords.distance(this.get_point_coords(point, null));

            if (this._side === St.Side.TOP ||
                this._side === St.Side.BOTTOM)
                this.emit('progress', offsetY);
            else
                this.emit('progress', offsetX);

            if (this._passesDistanceNeeded(point))
                this.set_state(Clutter.GestureState.COMPLETED);
        }
    }

    vfunc_point_ended(point) {
        this.set_state(Clutter.GestureState.CANCELLED);
    }

    vfunc_sequences_cancelled(sequences) {
        this.set_state(Clutter.GestureState.CANCELLED);
    }

    vfunc_state_changed(oldState, newState) {
        if (newState === Clutter.GestureState.CANCELLED ||
            newState === Clutter.GestureState.COMPLETED) {
            if (this._cancelTimeoutId) {
                GLib.source_remove(this._cancelTimeoutId);
                this._cancelTimeoutId = 0;
            }
        }
    }
});

export const EdgeSwipeTracker = GObject.registerClass({
    Properties: {
        'allow-swipe-anywhere': GObject.ParamSpec.boolean(
            'allow-swipe-anywhere', 'allow-swipe-anywhere', 'allow-swipe-anywhere',
            GObject.ParamFlags.READWRITE,
            false),
    },
}, class EdgeSwipeTracker extends SwipeTracker.SwipeTracker {
    _init(side, orientation, allowedModes, params) {
        super._init(orientation, allowedModes, params);

        this._side = side;
    }

    _getMonitorForCoords(coords) {
        const rect = new Mtk.Rectangle({x: coords.x - 1, y: coords.y - 1, width: 1, height: 1});
        const monitorIndex = global.display.get_monitor_index_for_rect(rect);

        return Main.layoutManager.monitors[monitorIndex];
    }

    _isNearMonitorEdge(point) {
        const { scaleFactor } = St.ThemeContext.get_for_stage(global.stage);
        const latestCoords = this.get_point_coords(point, null);
        const monitor = this._getMonitorForCoords(latestCoords);

        switch (this._side) {
        case St.Side.LEFT:
            return latestCoords.x < monitor.x + (EDGE_THRESHOLD * scaleFactor);
        case St.Side.RIGHT:
            return latestCoords.x > monitor.x + monitor.width - (EDGE_THRESHOLD * scaleFactor);
        case St.Side.TOP:
            return latestCoords.y < monitor.y + (EDGE_THRESHOLD * scaleFactor);
        case St.Side.BOTTOM:
            return latestCoords.y > monitor.y + monitor.height - (EDGE_THRESHOLD * scaleFactor);
        }
    }

    _exceedsBeginThreshold(point) {
        const beginCoords = this.get_point_begin_coords(point, null);
        const [distance] = beginCoords.distance(this.get_point_coords(point, null));

        return distance >= this.begin_threshold;
    }

    vfunc_point_began(point) {
        if (this.allowSwipeAnywhere) {
            super.vfunc_point_began(point);
            return;
        }

        if (this.state !== Clutter.GestureState.POSSIBLE) {
            super.vfunc_point_began(point);
            return;
        }

        if (!this._isNearMonitorEdge(point)) {
            this.set_state(Clutter.GestureState.CANCELLED);
            return;
        }

        this._cancelTimeoutId =
            GLib.timeout_add(GLib.PRIORITY_DEFAULT, CANCEL_TIMEOUT_MS, () => {
                if (this.state === Clutter.GestureState.POSSIBLE)
                    this.set_state(Clutter.GestureState.CANCELLED);

                delete this._cancelTimeoutId;
                return GLib.SOURCE_REMOVE;
            });

        super.vfunc_point_began(point);
    }

    vfunc_point_moved(point) {
        if (this._exceedsBeginThreshold(point)) {
            if (this._cancelTimeoutId) {
                GLib.source_remove(this._cancelTimeoutId);
                this._cancelTimeoutId = 0;
            }
        }

        super.vfunc_point_moved(point);
    }

    vfunc_state_changed(oldState, newState) {
        if (newState === Clutter.GestureState.CANCELLED ||
            newState === Clutter.GestureState.RECOGNIZE_PENDING ||
            newState === Clutter.GestureState.RECOGNIZING) {
            if (this._cancelTimeoutId) {
                GLib.source_remove(this._cancelTimeoutId);
                this._cancelTimeoutId = 0;
            }
        }

        super.vfunc_state_changed(oldState, newState);
    }
});
