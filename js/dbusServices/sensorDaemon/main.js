import {DBusService} from './dbusService.js';
import {SensorDaemon} from './sensorDaemon.js';
import {ProximityMonitoring} from './proximityMonitoring.js';
import {CallsMonitoring} from './callsMonitoring.js';

/** @returns {void} */
export async function main() {
    const sensorDaemon = new SensorDaemon();
    const proximityMonitoring = new ProximityMonitoring(sensorDaemon);
    sensorDaemon._proximityMonitoring = proximityMonitoring;
    const callsMonitoring = new CallsMonitoring(proximityMonitoring);

    const service = new DBusService('org.gnome.Shell.SensorDaemon', sensorDaemon);

    await service.runAsync();
}
