import Gio from 'gi://Gio';

import {loadInterfaceXML} from './misc/dbusUtils.js';
import * as ObjectManager from './misc/objectManager.js';

const CallIface = loadInterfaceXML('org.gnome.Calls.Call');
const CallProxy = Gio.DBusProxy.makeProxyWrapper(CallIface);

export const CallsMonitoring = class {
    constructor(proximityMonitoring) {
        this._proximityMonitoring = proximityMonitoring;

        this._objectManager = new ObjectManager.ObjectManager({
            connection: Gio.DBus.session,
            name: 'org.gnome.Calls',
            objectPath: '/org/gnome/Calls',
            knownInterfaces: [CallIface],
            onLoaded: this._onLoaded.bind(this),
        });

        this._inCall = false;
    }

    _onLoaded() {
        const callProxies = this._objectManager.getProxiesForInterface('org.gnome.Calls.Call');
        for (const proxy of callProxies)
            this._addCall(proxy);

        this._objectManager.connect('object-added', (objectManager, objectPath) => {
            if (objectPath.startsWith('/org/gnome/Calls/Call'))
                this._addCall(this._objectManager.getProxy(objectPath, 'org.gnome.Calls.Call'));
        });

        this._objectManager.connect('object-removed', (objectManager, objectPath) => {
            if (objectPath.startsWith('/org/gnome/Calls/Call'))
                this._removeCall(this._objectManager.getProxy(objectPath, 'org.gnome.Calls.Call'));
        });
    }

    _setInCall(inCall) {
        if (this._inCall === inCall)
            return;

        if (inCall)
            this._proximityMonitoring.startMonitoring();
        else
            this._proximityMonitoring.stopMonitoring();

        this._inCall = inCall;
    }

    _addCall(callProxy) {
        callProxy.connect('g-properties-changed', (proxy, properties) => {
            for (const prop in properties.deepUnpack()) {
                if (prop !== 'State')
                    return;

                // active, dialing, alerting states are considered in-call
                this._setInCall(callProxy.State === 1 || callProxy.State === 3 || callProxy.State === 4);
            }
        });

        if (callProxy.State === 1 || callProxy.State === 3 || callProxy.State === 4)
            this._setInCall(true);
    }

    _removeCall(callProxy) {
        this._setInCall(false);
    }
}
