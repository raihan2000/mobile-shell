# GNOME Shell Mobile
GNOME Shell Mobile is a set of patches on top of GNOME Shell to allow it to run on mobile phones and small tablets.

![](https://gitlab.gnome.org/verdre/mobile-shell/uploads/24687ebbb506b48af35eee3fa98668c3/mobileshell.png)

## Running it
This project consists of two parts, gnome-shell (this repo) and mutter (https://gitlab.gnome.org/verdre/mobile-mutter). Both need to be built in order to run GNOME Shell Mobile.

The `mobile-shell` branch is the main development branch. It's based on upstream `main` and can be used for nightly/unstable images.

For end users and distributors, there is the `gnome-44` branch which is based on the upstream stable `gnome-44` branch.

## Reporting bugs
Please report any issues with GNOME Shell Mobile on [this repo's issue tracker](https://gitlab.gnome.org/verdre/mobile-shell/-/issues), do not report them in the GNOME Shell issue tracker.

## Contributing
To contribute, please consider first if it makes sense to do so [upstream](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests), that is the case if the change is not specific to phones and can be based just fine on the upstream `main` branch.

Otherwise create a merge request for this repo targeting the `mobile-shell` branch.

## Upstreaming
It is the intention that all changes eventually land upstream in GNOME Shell and Mutter themselves. Due to the stability and code quality requirements that is going to take a while though, which is why this repo exists for everyone wanting to use the patches or contribute in the mean time.

## License
GNOME Shell is distributed under the terms of the GNU General Public License,
version 2 or later. See the [COPYING][license] file for details.

[license]: COPYING
